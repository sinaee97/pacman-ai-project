import util 

def BrutForce(self,node,see) :
	for i in range(0,32) :
		for j in range(0,32):
			if (node[i][j] == 0) and (see[i][j] == 0):
				see[i][j] = 1
				print(i , j)
				self.player.center_x = 20 * j
				self.player.center_y = 20 * (31 - i)
				return


def dfs(node,start) :
	ret = []
	move = ""
	col = [0,0,-1,1]
	row = [1,-1,0,0]
	stack = util.Stack()
	see = [[0 for x in range(32)] for y in range(32)]
	par = [[0 for x in range(32)] for y in range(32)]
	stack.push(start)
	par[start[0]][start[1]] = ((0,0),"Start")

	while(stack.isEmpty() == False) :
		top = stack.pop()
		see[top[0]][top[1]] = 1
		if util.isGoalState(node,top) == True :
			break
		for i in range(0,4):
			newx = row[i] + top[0] 
			newy = col[i] + top[1]
			if (see[newx][newy] == 0) and (node[newx][newy] != 1):
				if newx > top[0] :
					move = "D"
				elif newx < top[0] :
					move = "U"
				elif newy > top[1] :
					move = "R"
				else :
					move = "L"
				par[newx][newy] = (top,move)
				stack.push((newx,newy))
	cur = top
	while par[cur[0]][cur[1]] != ((0,0),"Start") :
		ret.append(par[cur[0]][cur[1]][1])
		cur = par[cur[0]][cur[1]][0]
	return ret			


def bfs(node,start) :
	ret = []
	move = ""
	col = [0,0,-1,1]
	row = [1,-1,0,0]
	queue = util.Queue()
	see = [[0 for x in range(32)] for y in range(32)]
	par = [[0 for x in range(32)] for y in range(32)]
	queue.push(start)
	par[start[0]][start[1]] = ((0,0),"Start")
	see[start[0]][start[1]] = 1
	while(queue.isEmpty() == False) :
		top = queue.pop()
		if util.isGoalState(node,top) == True :
			break
		for i in range(0,4):
			newx = row[i] + top[0] 
			newy = col[i] + top[1]
			if (see[newx][newy] == 0) and (node[newx][newy] != 1):
				if newx > top[0] :
					move = "D"
				elif newx < top[0] :
					move = "U"
				elif newy > top[1] :
					move = "R"
				else :
					move = "L"
				par[newx][newy] = (top,move)
				see[newx][newy] = 1
				queue.push((newx,newy))
	cur = top
	while par[cur[0]][cur[1]] != ((0,0),"Start") :
		ret.append(par[cur[0]][cur[1]][1])
		cur = par[cur[0]][cur[1]][0]
	return ret

